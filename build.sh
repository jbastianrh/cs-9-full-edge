#!/bin/bash
#
#	build a full edge kernel from the MRs (or other sources) that
#	we know about.
#

#-- We assume we are in a git tree cloned from:
#
#	git@gitlab.com:ahs3/cs-9-full-edge.git
#
# and that ./setup.sh has already been run
#

function do_merge () {
	mr=$1
	style=$2

	msg=$(git log --oneline -n 50 | grep "Merge remote-tracking branch 'centos9/merge-requests/$mr' into")
	if [ -z "$msg" ]
	then
		echo -e "\n==> Adding MR !$mr ... "
		if [ $style == "none" ]
		then
			git merge --signoff centos9/merge-requests/$mr

		elif [ $style == "ours" ]
		then
			git merge --signoff -X ours centos9/merge-requests/$mr

		elif [ $style == "theirs" ]
		then
			git merge --signoff -X theirs centos9/merge-requests/$mr

		fi
		[ $? -ne 0 ] && exit 1
	else
		echo -e "==> MR !$mr already added ... "
	fi
}

#-- start in the right place
git checkout build

#-- what shall we call the localversion?
todays_date=$(date +"%Y%m%d")
full_version=".edge.$todays_date"
echo -e "\n==> Version: $full_version"
echo $full_version > localversion

#-- create a new branch for this new version
echo -e "\n==> Create today's build branch ..."
if [ ! -z "$(git branch | grep $todays_date)" ]
then
	git branch -D $todays_date
fi
git checkout -b $todays_date build

#-- apply all the MRs we know about and think are potentially useful
#   please keep these in numerical order so things can be found.
#
#   NB: this list will need to change as MRs get pulled in or have
#   their status changed in some way.
#
#   Order of MRs being applied is important.
#

#-- potential MRs:
#   "1677 theirs" => this is still Draft, causes compilation problems
#   "1817 none"   => currently causing compilation errors
declare -a mr_list
mr_list=(\
   "1409 none" "1438 ours" "1439 none" "1484 none" \
   "1531 none" "1584 none" \
   "1621 none" \
   "1738 none" "1771 theirs" "1782 none" "1790 none" \
   "1818 none" "1838 none" \
)
echo -e "\n==> Merging MRs to today's build branch ..."
for ii in ${!mr_list[@]}
do
	do_merge ${mr_list[ii]} none
done

#-- merge in msalter patches
echo -e "\n==> Merging tegra234 to today's build branch ..."
git merge --signoff tegra234
[ $? -ne 0 ] && exit 1

#-- apply any custom patches (i.e., everything in ./custom-patches/*.patch
#
#   Please maintain alphanumeric order here.
#
echo -e "\n==> Applying custom patches to today's build branch ..."
if [ -d custom-patches ]
then
	if [ ! -z "$(cd custom-patches; ls *.patch 2>/dev/null)" ]
	then
		for ii in custom-patches/*.patch
		do
			git am $ii
			[ $? -ne 0 ] && exit 1
		done
	fi
fi

echo -e "\n==> All done ..."
echo "   Build the kernel with './dobrew.sh' or this command:"
echo "   BUILD_FLAGS=\"--arch-override=aarch64\" make dist-brew"

exit 0
