#!/bin/bash
#
#	once cloned, set up the repo in the manner to which we would
#	like to be accustomed
#
# The clone needs to be from here:
#
#	git@gitlab.com:ahs3/cs-9-full-edge.git
#
# and we need to be in the default branch (build).
#

#-- add all the remotes we want
echo -e "\nAdd remotes ..."
git remote add centos9 \
    https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git
git remote add msalter https://gitlab.com/msalter/centos-stream-9.git

#-- add in a config item we know we'll probably need
echo -e "\nAdd config item to see merge requests ..."
git config --add \
    remote.centos9.fetch \
    "+refs/merge-requests/*/head:refs/remotes/centos9/merge-requests/*"

#-- grab copies of it all
echo -e "\nBring repo up to date ..."
git fetch --all
git checkout build
git pull

#-- basic branch setup
echo -e "\nSet up initial branches ..."
git checkout -b centos9-main centos9/main
git checkout -b main origin/main
git checkout -b tegra234 msalter/tegra234

#-- update everything
echo -e "\n==> Fetch all the repos ..."
git fetch --all

echo -e "\n==> Update specific branches ..."
for ii in centos9-main main build
do
	git checkout $ii
	git pull
done

echo -e "\n==> Make sure main is current with cs9 main ..."
git checkout main
git merge centos9-main

echo -e "\n==> Make sure build is current with main ..."
git checkout build
git merge main

#-- make sure we're in the right branch
echo -e "\n==> Start work in the build branch ..."
git checkout build

#-- and all done
echo -e "\nAll done."
git checkout build

exit 0
